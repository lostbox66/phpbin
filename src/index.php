<?php 
    include 'config.php';
?>
<title><?php echo $sitename; ?></title>
<link rel="icon" type="image/x-icon" href="/favicon.ico">
<?php
    $theme = $_COOKIE["theme"];
    if ($theme == "default") {
      echo "<link rel='stylesheet' href='style.css'>";
    } elseif ($theme == "metal") {
      echo "<link rel='stylesheet' href='metal.css'>";
    } else {
      echo "<link rel='stylesheet' href='style.css'>";
    }
?>
<h1><?php echo $sitename; ?></h1>
<?php echo $sitedesc; ?>
<br>
<?php
  if ($sitenoteenable == 1) {
    echo $sitenote;
  }
?>
<hr>
<form action="cookie.php" method="get">
  <label for="theme">Theme: </label>
  <select name="rtheme" id="rtheme">
    <option value="default">Default (Yotsuba)</option>
    <option value="metal">Metal.css</option>
  </select>
  <input type="submit" value="Apply">
</form>
<br>

<?php 

if ($enable_captcha == 1) {
  echo '
  <form method="post" action="post.php">
    <br>
    <img src="./captcha.php" alt="CAPTCHA Image" id="captcha-img" width=250px>
    <br>
    <input type="text" id="captcha_text" name="captcha_text">
    <input type="submit" value="Submit">
    <textarea name="paste">'.htmlspecialchars($_POST["paste"]).'</textarea>
  </form>
  ';
} else {
  echo '
  <form method="post" action="post.php">
    <br>
    <input type="submit" value="Submit">
    <textarea name="paste">'.htmlspecialchars($_POST["paste"]).'</textarea>
  </form>
  ';
}


?>