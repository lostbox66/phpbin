<?php
session_start();

// Generate random text for the CAPTCHA
$text = substr(sha1(rand()), 0, 7);

// Store the text inf the session so we can check it later
$_SESSION['captcha_text'] = $text;

// Create the image
$image = imagecreatetruecolor(175, 40);
$bg_color = imagecolorallocate($image, 255, 255, 255);
$text_color = imagecolorallocate($image, 0, 0, 0);

// Fill the background with white
imagefilledrectangle($image, 0, 0, 175, 40, $bg_color);

// Draw random lines on the image with random color
for ($i = 0; $i < 10; $i++) {
    $color = imagecolorallocate($image, rand(0, 255), rand(0, 255), rand(0, 255));
    imageline($image, 0, rand() % 40, 175, rand() % 40, $color);
}

// Distort the text by applying random perturbations
$font = './incon.ttf';
for ($i = 0; $i < strlen($text); $i++) {
    $x = 20 + ($i * 20);
    $y = rand(20, 30);
    imagettftext($image, 18, rand(-10, 10), $x, $y, $text_color, $font, $text[$i]);
}

// Output the image
header('Content-Type: image/png');
imagepng($image);

// Clean up
imagedestroy($image);
?>
