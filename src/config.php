<?php
    // Configuration for phpbin
    
    $sitename = "phpbin";
    // Note: sitedesc, by default, is rendered with HTML.
    // This is for more customization, and because it can _only_
    // be set by the admins, so there's very little hackability.
    $sitedesc = "a <i>really simple</i> pastebin in PHP";
    
    // Enable or disable the "note" that comes below the sitename & description
    $sitenoteenable = 0; // 1 is enabled, 0 (or anything else, really) is disabled.
    // This goes with the above option. Basically like sitedesc but longer
    // and more drawn out. Not really necessary, but neat.
    // If you're familiar with PHP, you can use your sitename (or other variables)
    // within this to give it a more fleshed-out feel. I don't know, heh.
    $sitenote = $sitename." is a simple pastebin written in PHP. Get pasting!";
    
    // I recommend keeping this set to 1 if this site is intended to be public.
    // Set it to 2 to disable the captcha. Other values will probably break it
    $enable_captcha = "1";
?>