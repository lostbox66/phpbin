<?php
include 'config.php';
?>
<?php
    $theme = $_COOKIE["theme"];
    if ($theme == "default") {
      echo "<link rel='stylesheet' href='style.css'>";
    } elseif ($theme == "metal") {
      echo "<link rel='stylesheet' href='metal.css'>";
    } else {
      echo "<link rel='stylesheet' href='style.css'>";
    }
?>
<?php

if ($enable_captcha == "1") { // Captchas ON
  // echo "captchas are enabled";
  session_start();
  
  // Get the form data
  $paste = $_POST['paste'];
  $captcha_text = $_POST['captcha_text'];
  
  
  // Check if the text entered by the user matches the text stored in the session
  if ($captcha_text == $_SESSION['captcha_text']) {
    // CAPTCHA is correct, proceed with the normal stuff.
    $n = 16;
    function getRandomString($n)
    {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    
    for ($i = 0; $i < $n; $i++) {
      $index = rand(0, strlen($characters) - 1);
      $randomString .= $characters[$index];
    }
    
    return $randomString;
    }
    
    $paste = $_POST["paste"];
    
    $filename = hash('sha1', $paste);
    echo "<b>Hold on while I ppppprocess this</b>";
    
    $myfile = fopen("paste/".$filename.".txt", "w") or die("Unable to open file!");
    $txt = $paste;
    fwrite($myfile, $txt);
    fclose($myfile);
  
    echo " <meta http-equiv=\"refresh\" content=\"0; url='/view.php?file=".$filename."'\" />";
  } else {
    // CAPTCHA text does not match, return error
    echo "<h1>Error: Invalid CAPTCHA text.</h1>";
    echo " <meta http-equiv=\"refresh\" content=\"5; url='/'\" />";
    echo "<h2>Redirecting to home in 5 seconds...</h2>";
  }

} elseif ($enable_captcha == "2") { // Captchas OFF
  // echo "captchas are disabled";
  $n = 16;
  function getRandomString($n)
  {
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $randomString = '';
  
  for ($i = 0; $i < $n; $i++) {
    $index = rand(0, strlen($characters) - 1);
    $randomString .= $characters[$index];
  }
  
  return $randomString;
  }
  
  $paste = $_POST["paste"];
  
  $filename = hash('sha1', $paste);
  echo "<b>Hold on while I ppppprocess this</b>";
  
  $myfile = fopen("paste/".$filename.".txt", "w") or die("Unable to open file!");
  $txt = $paste;
  fwrite($myfile, $txt);
  fclose($myfile);

  echo " <meta http-equiv=\"refresh\" content=\"0; url='/view.php?file=".$filename."'\" />";
}


?>