PHPbin - a really damn simple pastebin in PHP

Note: I haven't checked this for security flaws/risks.
It has been cleared of any basic exploits with
htmlspecialchars().

I recommend you configure your webserver so the /paste/
directory can't be viewed normally. I did create an index.php
file in there to handle this, but you should set that up
anyway. It's good practice :)

Enjoy

~ lostbox66 ~